package com.example.movieappmvp.movieDetails.di

import ccom.example.movieappmvp.movieDetails.MovieDetailsActivity

import com.example.movieappmvp.movieDetails.bl.GetMovieDetailsApi

import com.example.movieappmvp.movies.adapter.MovieItemLayout
import com.example.movieappmvp.movies.di.MoviesComponent
import com.example.movieappmvpmovieDetails.trailer.bl.GetMovieVideosApi
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import retrofit2.Retrofit
import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MovieDetailsScope


@MovieDetailsScope
@Subcomponent(modules = [MovieDetailsBindsModule::class, MovieDetailsProvidesModule::class])
interface MovieDetailsComponent {

    fun inject(layout: MovieItemLayout)

    fun inject(activity: MovieDetailsActivity)

    @Subcomponent.Builder
    interface Builder {

        fun build(): MovieDetailsComponent
    }
}

@Module
interface MovieDetailsBindsModule

@Module
class MovieDetailsProvidesModule {

    @Provides
    fun providesMovieDetailsApi(retrofit: Retrofit): GetMovieDetailsApi {
        return retrofit.create(GetMovieDetailsApi::class.java)
    }

    @Provides
    fun providesMovieVideosApi(retrofit: Retrofit): GetMovieVideosApi {
        return retrofit.create(GetMovieVideosApi::class.java)
    }
}
