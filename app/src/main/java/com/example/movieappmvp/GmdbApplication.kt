package com.example.movieappmvp

import android.app.Application
import com.example.movieappmvp.di.ApplicationComponent
import com.example.movieappmvp.di.DaggerApplicationComponent
import dagger.android.DaggerApplication

import timber.log.Timber

class GmdbApplication : Application() {

    companion object {
        private lateinit var application: GmdbApplication

        fun getInstance(): GmdbApplication {
            return application
        }
    }

    lateinit var appComponent: ApplicationComponent
        private set

    override fun onCreate() {
        super.onCreate()
        application = this
        Timber.plant(Timber.DebugTree())

        appComponent = DaggerApplicationComponent
            .builder()
            .application(this)
            .build()
        appComponent.inject(this)
    }
}