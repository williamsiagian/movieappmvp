package com.example.movieappmvp.movies.adapter

import com.example.movieappmvp.utils.DelegatedAdapter
import com.example.movieappmvp.utils.TypedAdapterDelegate
import com.example.movieappmvp.utils.ViewHolderRenderer

class MoviesAdapter(
    private var itemClickListener: OnMovieClick
) : DelegatedAdapter() {

    companion object {
        // TODO - add type loader when infinite scrolling will be implemented
        const val TYPE_MOVIE = 0
    }

    init {
        addDelegate(TYPE_MOVIE, TypedAdapterDelegate { parent ->
            ViewHolderRenderer(MovieItemLayout(parent.context).apply {
                setOnItemClickListener(itemClickListener)
            })
        })
    }

    override val items: MutableList<Int> = mutableListOf()

    override fun getItemViewType(position: Int): Int = TYPE_MOVIE

    fun updateDataSet(updatedList: List<Int>) {
        items.clear()
        items.addAll(updatedList)
        notifyDataSetChanged()
    }
}

typealias OnMovieClick = (movieId: Int) -> Unit
