package com.example.movieappmvp.movies

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ccom.example.movieappmvp.movieDetails.MovieDetailsActivity
import com.example.movieappmvp.GmdbApplication
import com.example.movieappmvp.R
import com.example.movieappmvp.movies.adapter.MoviesAdapter
import com.example.movieappmvp.utils.hide
import com.example.movieappmvp.utils.kotterknife.bindView
import com.example.movieappmvp.utils.setScreenOrientation
import com.example.movieappmvp.utils.show
import javax.inject.Inject

class MoviesActivity : AppCompatActivity(), MoviesView {

    private val moviesRecycler: RecyclerView by bindView(R.id.movies_recycler)
    private val retry: View by bindView(R.id.movies_retry)
    private val loader: View by bindView(R.id.movies_loader)

    private val adapter = MoviesAdapter {
        startActivity(
            Intent(this, MovieDetailsActivity::class.java)
                .apply { putExtra(MovieDetailsActivity.MOVIE_ID, it) }
        )
    }

    @Inject
    lateinit var presenter: MoviesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setScreenOrientation()
        setContentView(R.layout.activity_movies)

        bindViews()
        inject()
    }

    private fun bindViews() {
        val layoutManager = GridLayoutManager(this, resources.getInteger(R.integer.grid_movie_span))
        moviesRecycler.layoutManager = layoutManager
        moviesRecycler.adapter = adapter

        // TODO implement picker for date ranges and pass it to the presenter
        retry.setOnClickListener { presenter.loadMovieChanges() }
    }

    private fun inject() {
        GmdbApplication.getInstance().appComponent
            .moviesBuilder()
            .build()
            .inject(this)

        presenter.attach(this)
        // TODO implement picker for date ranges and pass it to the presenter
        presenter.loadMovieChanges()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isFinishing) {
            presenter.detach()
        }
    }

    override fun showProgress() {
        loader.show()
        moviesRecycler.hide()
        retry.hide()
    }

    override fun error(message: String) {
        loader.hide()
        moviesRecycler.hide()
        retry.show()
    }

    override fun movieList(list: List<Int>) {
        loader.hide()
        moviesRecycler.show()
        retry.hide()

        adapter.updateDataSet(list)
    }
}
